import React from 'react'
import "./styles.css"

// this component is styled with normal CSS (as an example)

export default function FooterComponent() {
  return (
    <footer className='footer'>
      <h1>2021 AND Digital - Privacy — Terms</h1>
      <h2>We accelerate digital capabilities</h2>
    </footer>
  )
}
